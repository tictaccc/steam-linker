steam-linker is a small linux utility for creating a directory of symlinks relevent to installed steam games. No more hunting down a game's app ID to find it's proton prefix!

### Created structure
- `$HOME/.steam-links`
  - `Game Name - STEAM_APP_ID`
    - `prefix` -> `...steamapps/compatdata/STEAM_APP_ID/prefix/drive_c/...`
    - `files` -> `...steamapps/common/INSTALL FOLDER`
    - `config` -> `...`
  
- `prefix` Should always be set for proton games
- `files` Is set if the folder name matches the game name or is defined in `installs.txt`
- `config` Is defined in `configs.txt`
